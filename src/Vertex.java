
public class Vertex {

	private float x,y,z;
	
	public Vertex(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vertex() {
		x = 0;
		y = 0;
		z = 0;
	}
	
	public void setX(float value) {
		x = value;
	}
	
	public void setY(float value) {
		y = value;
	}

	public void setZ(float value) {
		z = value;
	}
	
	public float x() {
		return x;
	}
	
	public float y() {
		return y;
	}

	public float z() {
		return z;
	}
}
