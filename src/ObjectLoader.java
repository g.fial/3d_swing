	import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


	public class ObjectLoader {
	    
	    private final static String OBJ_VERTEX = "v";
	    private final static String OBJ_VERTEX_TEXTURE = "vt";
	    private final static String OBJ_FACE = "f";
	    
	    FileReader fileReader;

	    public ObjectLoader() {
	        //...
	    }

		public Vertex[][] load(File objFile) throws IOException {
					
			fileReader = new FileReader(objFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			int lineCount = 0;
			int vertexCount = 0;
			int vTextureCount = 0;
			int faceCount = 0;
			
			String line = null;
			while(true) {
				line = bufferedReader.readLine();
				if(null == line) {
					break;
				}
				
				line = line.trim();
				
				if(line.length() == 0)
					continue;
				
				String tokens[] = line.split("[\t ]+");

	            if(line.startsWith("#")) {
	                continue;
	            }
	            else if(tokens[0].equals(OBJ_VERTEX)) {
	                // parse vertex line
	            	if(vertexCount >= obj[0].length - 1)
	            		duplicateMemory(0);
	            	
	            	obj[0][vertexCount++] = new Vertex(Float.parseFloat(tokens[1]),Float.parseFloat(tokens[2]),Float.parseFloat(tokens[3]));
				}
	            else if(tokens[0].equals(OBJ_VERTEX_TEXTURE)) {
					// parse texture coordinates
	            	if(vTextureCount >= obj[1].length - 1)
	            		duplicateMemory(1);
	            	
	            	obj[1][vTextureCount++] = new Vertex(Float.parseFloat(tokens[1]),Float.parseFloat(tokens[2]), 0);
				}
				else if(tokens[0].equals(OBJ_FACE)) {
					// parse face information
					/*
					if(faceCount >= obj[2].length - 3)
	            		duplicateMemory(2);
					
					for(int i = 1; i <= 3; i++){
						String faceV[] = tokens[i].split("/");
						obj[2][faceCount] = new Vertex();
						
						// Para cada "vertice" de face >x/x/x< y/y/y z/z/z
						for(int j = 1; j <= 3; j++) {
							
							if(faceV[j] == null)
								break;
							
							if(j == 1)
								obj[2][faceCount].setX(Float.parseFloat(faceV[j]));
								System.out.println("X set");
							if(j == 2)
								System.out.println("obj[2] -> "+obj[2].length+ ", faceCount -> "+faceCount);
								obj[2][faceCount].setY(Float.parseFloat(faceV[j]));
							
							if(j == 3)
								obj[2][faceCount].setZ(Float.parseFloat(faceV[j]));
							
							faceCount++;
						}
					}
					*/
				}
					
	            lineCount++;
	        }
			
	        bufferedReader.close();
	        System.out.println("There are "+vertexCount+" vertices.");
	        System.out.println("There are "+vTextureCount+" texture vertices.");
	        System.out.println("There are "+(faceCount/3)+" faces.");
	        System.err.println("Loaded " + lineCount + " lines");
			return obj;
		}
		
		public Vertex[] getVertices() {
			return null;
		}
		
		public Vertex[] getVerticesTexture() {
			return null;
		}
		
		public List<List<Vertex>> getFaces() {
			return null;
		}
		
		private void duplicateMemory(int type) {
			System.out.print("Duplicating Memory: "+obj[0].length+" to ");
			Vertex[] aux = new Vertex[obj[type].length*2];
			
			for(int i = 0; i < obj[type].length; i++){
				aux[i] = obj[type][i];
			}
			
			System.out.println(aux.length + " positions");
			
			obj[type] = aux;
		}
		
		Vertex[][] obj = new Vertex[3][1024];

	}
