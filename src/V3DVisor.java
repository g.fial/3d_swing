import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;


public class V3DVisor extends GLCanvas implements GLEventListener, KeyListener {

	private static final long serialVersionUID = 1L;
	private GLAutoDrawable glDraw;
	private double zoom = 1;
	private boolean boundingBox = true;
	private float alpha = (float) 63.4;
	private float l = (float) 0.5;
	private float a = (float) 24.46;
	private float b = 17;
	private int d = 50;
	private float gamma;
	private float theta2;
	private float theta = 60;
	private int ortoType = 1;
	private int projection = 1;
	private Vertex[] vertices;
	
	public V3DVisor() {
		
		addGLEventListener(this);
		setSize(600, 600);
		requestFocusInWindow();
	}
	
	@Override
	public void display(GLAutoDrawable gLDrawable) {
		
		GL gl = gLDrawable.getGL();
		
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();
		
		double aspect = (double)getWidth()/(double)getHeight();
		
		if(aspect < 1)
			gl.glOrtho(-2.0/zoom, 2.0/zoom, (-2.0/zoom)/aspect, (2.0/zoom)/aspect, -2.0/zoom, 2.0/zoom);
		
		else {
			gl.glOrtho((-2.0/zoom)*aspect, (2.0/zoom)*aspect, -2.0/zoom, 2.0/zoom, (-2.0/zoom)*aspect, (2.0/zoom)*aspect);
		}

		switch(projection){
		case 1: this.applyOrtogonalMatrix(gl); break;
		case 2: this.applyOlbiqueMatrix(gl); break;
		case 3: this.applyOrtogonalMatrix(gl); break;
		case 4: this.applyPerpectiveMatrix(gl, d);
		}

		if(projection == 1){
			switch(ortoType){
			case 2: theta = 90; this.applyXRotation(gl, theta); break; //Planta
			case 3: theta = 90; this.applyYRotation(gl, theta); break; //Al�ado lateral esquerdo
			case 4: theta = -90; this.applyYRotation(gl, theta); //Al�ado lateral direito
			}
		}

		if(projection==3){
			theta2 = (float) (Math.atan(Math.sqrt(Math.tan(Math.PI*a/180)/Math.tan(Math.PI*b/180)))-(Math.PI/2));
			gamma = (float) Math.asin(Math.sqrt((Math.tan(Math.PI*a/180)*Math.tan(Math.PI*b/180))));
			//this.applyYRotation(gl, gamma);
			//this.applyXRotation(gl, theta2);
			gl.glRotatef(theta2, 1, 0, 0);
			gl.glRotatef(1, gamma, 0, 0);
			

		}
		
		if(vertices != null) {
		
			gl.glBegin( GL.GL_LINES );
			gl.glColor3f(1.0f, 0.0f, 0.0f); // RED
			gl.glVertex3f( 0.0f, 0.0f, 0.0f );
			gl.glVertex3f( 1.0f, 0.0f, 0.0f );
			gl.glColor3f(0.0f, 1.0f, 0.0f); // GREEN
			gl.glVertex3f( 0.0f, 0.0f, 0.0f );
			gl.glVertex3f( 0.0f, 1.0f, 0.0f );
			gl.glColor3f(0.0f, 0.0f, 1.0f); // BLUE
			gl.glVertex3f( 0.0f, 0.0f, 0.0f );
			gl.glVertex3f( 0.0f, 0.0f, 1.0f );
			gl.glEnd( );
			gl.glFlush();
			
			
		
			boundingBox(gl);
			fitObject1x1x1(gl);
			gl.glBegin(GL.GL_POINTS);
			gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
			gl.glColor3f(1.0f, 1.0f, 1.0f);
			
		
			for(int i = 0; vertices[i] != null; i++) {			
				gl.glVertex3f( vertices[i].x(), vertices[i].y(), vertices[i].z());
			}
		
			gl.glEnd();
			gl.glFlush();
		
		}
		/*
		GLUT glut = new GLUT();
		glut.glutWireTeapot(1);
		*/
	}

	@Override
	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
		
	}

	@Override
	public void init(GLAutoDrawable gLDrawable) {
		
		glDraw = gLDrawable;
		GL gl = gLDrawable.getGL();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glDraw.addKeyListener(this);
		
	}

	@Override
	public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width, int height) {

		GL gl = gLDrawable.getGL();
		gl.glViewport(0, 0, width, height); // altera-se o visor
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent e) {
		
		if (e.getKeyChar() == '\u001B' ||  // escape
				e.getKeyChar() == 'Q' ||
				e.getKeyChar() == 'q') {   // quit
			System.exit(0);
		}
		if(e.getKeyChar() == '+'){
			zoom = zoom + 0.1;
			reDraw();
		}
		if(e.getKeyChar() == '-'){
			zoom = zoom -0.1;
			reDraw();
		}
	}

	private void applyXRotation(GL gl, float angle){
		float[] matrix = { 1, 0, 0, 0,
				0, (float) Math.cos(Math.PI*angle/180), (float) - Math.sin(Math.PI*angle/180), 0,
				0, (float) Math.sin(Math.PI*angle/180), (float) Math.cos(Math.PI*angle/180),0,
				0, 0, 0, 1
		};

		gl.glMultMatrixf(matrix, 0);
	}

	private void applyYRotation(GL gl, float angle){
		float[] matrix = {(float) Math.cos(Math.PI*angle/180), 0, (float) Math.sin(Math.PI*angle/180), 0,
				0, 1, 0, 0,
				(float) - Math.sin(Math.PI*angle/180), 0, (float) Math.cos(Math.PI*angle/180), 0,
				0, 0, 0, 1
		};
		gl.glMultMatrixf(matrix, 0);
	}

	private void applyOrtogonalMatrix(GL gl){
		float[] matrix =  {1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 1};

		gl.glMultMatrixf(matrix, 0);

	}

	private void applyPerpectiveMatrix(GL gl, int d){
		float[] matrix =  {1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 0, (float)-1/d,
				0, 0, 0, 1};
		gl.glMultMatrixf(matrix, 0);
	}

	private void applyOlbiqueMatrix(GL gl){
		float[] matrix = {1, 0, 0, 0,
				0, 1, 0, 0,
				(float) (-l*Math.cos(Math.PI*alpha/180)), (float) (-l* Math.sin(Math.PI*alpha/180)), 0, 0,
				0, 0, 0, 1};

		gl.glMultMatrixf(matrix, 0);
	}
	
	private void fitObject1x1x1(GL gl) {
		
		if(vertices[0] == null)
			return;
		
		float maxX=0, minX=0, maxY=0, minY=0, maxZ=0, minZ=0, widerSide = 0;
		Vertex center = new Vertex();

		for(int i = 0; vertices[i] != null; i++) {

			if(i == 0){
				minX = vertices[i].x();
				minY = vertices[i].y();
				minZ = vertices[i].z();
				maxX = vertices[i].x();
				maxY = vertices[i].y();
				maxZ = vertices[i].z();
			}

			if(maxX <= vertices[i].x())
				maxX = vertices[i].x();

			if(minX > vertices[i].x())
				minX = vertices[i].x();

			if(maxY <= vertices[i].y())
				maxY = vertices[i].y();

			if(minY > vertices[i].y())
				minY = vertices[i].y();
			
			if(maxZ <= vertices[i].z())
				maxZ = vertices[i].z();
			
			if(minZ > vertices[i].z())
				minZ = vertices[i].z();


		}

		// Aqui sao guardadas as larguras de cada lado
		center.setX((float)Math.sqrt(Math.pow(minX - maxX, 2)));
		center.setY((float)Math.sqrt(Math.pow(minY - maxY, 2)));
		center.setZ((float)Math.sqrt(Math.pow(minZ - maxZ, 2)));
		
		// Saber qual o maior lado
		widerSide = center.x();
		
		if(center.y() > widerSide)
			widerSide = center.y();
		
		if(center.z() > widerSide)
			widerSide = center.z();

		// Se x1 < x2, somar a X a metade da diferenca
		if(minX <= maxX) {
			center.setX(center.x()/2);
			center.setX(minX + center.x());
		}

		// Se x1 > x2, subtrair a X a metade da diferenca
		else {
			center.setX(center.x()/2);
			center.setX(minX - center.x());
		}

		// Se y1 < y2, somar a Y a metade da diferenca
		if(minY <= maxY) {
			center.setY(center.y()/2);
			center.setY(minY + center.y());
		}

		// Se y1 > y2, subtrair a Y a metade da diferenca
		else {
			center.setY(center.y()/2);
			center.setY(minY - center.y());
		}
		
		// Se z1 < z2, somar a Z a metade da diferenca
		if(minZ <= maxZ) {
			center.setZ(center.z()/2);
			center.setZ(minZ + center.z());
		}
				
		// Se Z1 > Z2, subtrair a Z a metade da diferenca
		else {
			center.setZ(center.z()/2);
			center.setZ(minZ - center.z());
		}
		
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glScalef(1/widerSide,1/widerSide,1/widerSide);
		gl.glTranslatef(-center.x(), -center.y(), -center.z());
		
	}
	
	private void boundingBox(GL gl) {
		
		if(!boundingBox)
			return;
		
		gl.glBegin(GL.GL_LINE_STRIP);
		gl.glColor3f(0.6f, 0.6f, 0.6f);
		gl.glVertex3f( -0.5f, -0.5f, -0.5f );
		gl.glVertex3f( -0.5f, -0.5f, 0.5f );
		gl.glVertex3f( 0.5f, -0.5f, 0.5f );
		gl.glVertex3f( 0.5f, -0.5f, -0.5f );
		gl.glVertex3f( -0.5f, -0.5f, -0.5f );
		gl.glVertex3f( -0.5f, 0.5f, -0.5f );
		gl.glVertex3f( -0.5f, 0.5f, 0.5f );
		gl.glVertex3f( 0.5f, 0.5f, 0.5f );
		gl.glVertex3f( 0.5f, 0.5f, -0.5f );
		gl.glVertex3f( -0.5f, 0.5f, -0.5f );
		gl.glEnd();
		
		gl.glBegin(GL.GL_LINES);
		gl.glColor3f(0.6f, 0.6f, 0.6f);
		gl.glVertex3f( -0.5f, -0.5f, -0.5f );
		gl.glVertex3f( -0.5f, 0.5f, -0.5f );
		gl.glVertex3f( 0.5f, -0.5f, 0.5f );
		gl.glVertex3f( 0.5f, 0.5f, 0.5f );
		gl.glVertex3f( -0.5f, -0.5f, 0.5f );
		gl.glVertex3f( -0.5f, 0.5f, 0.5f );
		gl.glVertex3f( 0.5f, -0.5f, -0.5f );
		gl.glVertex3f( 0.5f, 0.5f, -0.5f );
		gl.glEnd();
	}
	
	public void toggleBoundingBox() {
		boundingBox = !boundingBox;
	}
	
	public void load(File file) throws IOException {
		ObjectLoader loader = new ObjectLoader();
	
		vertices = loader.load(file)[0];
		glDraw.display();
	}
	
	public void clear() {
	
		vertices = null;
		glDraw.display();
	}
	
	public void setProjection(int p) {
		projection = p;
	}
	
	public void setOrtoType(int t) {
		ortoType = t;
	}
	
	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
	
	public float getAlpha() {
		return alpha;
	}
	
	public void setL(float l) {
		this.l = l;
	}
	
	public float getL() {
		return l;
	}
	
	public void setA(float a) {
		this.a = a;
	}
	
	public float getA() {
		return a;
	}
	
	public void setB(float b) {
		this.b = b;
	}
	
	public float getB() {
		return b;
	}
	
	public void setD(int d) {
		this.d = d;
	}
	
	public float getD() {
		return d;
	}
	
	public void reDraw() {
		glDraw.display();
	}
	
	/*
	 * Para aplicar no reshape se quisermos manter aspect ratio com base em viewport:
	 * 
	 * if(width/height < 1)
			gl.glViewport(0, (height-width)/2, width, width);
		else
			gl.glViewport((width-height)/2, 0, height, height); // altera-se o visor
	 */

}
