import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		V3DFrame frame = new V3DFrame("Visualizador de Objectos 3D");
		frame.setVisible(true);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
	}

}
