import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class V3DFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private V3DVisor visor;
	private JMenuBar bar = new JMenuBar();
	private final int MAXL = 100;
	private final int MINL = 0;
	private final int DEFL = 50;
	
	public V3DFrame(String title) {
		
		super(title);
		visor = new V3DVisor();
		add(visor, BorderLayout.CENTER);
		
		bar.add(createFileMenu());
		JTabbedPane painel = createPanel();
		
		add(painel, BorderLayout.SOUTH);
		add(bar, BorderLayout.NORTH);
		pack();
	}
	
	public JTabbedPane createPanel() {
		final JTabbedPane tabbedPane = new JTabbedPane();
		JPanel oblique = createObliquePanel();
		JPanel axionometric = createAxionometricPanel();
		JPanel ortogonal = createOrtogonalPanel();
		JPanel perspective = createPerspectivePanel();
		tabbedPane.add("Ortogonal", ortogonal);
		tabbedPane.add("Obl�qua", oblique);
		tabbedPane.add("Axionom�trica", axionometric);
		tabbedPane.add("Perspectiva", perspective);


		class CListener implements ChangeListener{

			@Override
			public void stateChanged(ChangeEvent e) {
				switch(tabbedPane.getSelectedIndex()){
				case 0: visor.setProjection(1); break;
				case 1: visor.setProjection(2); break;
				case 2: visor.setProjection(3); break;
				case 3: visor.setProjection(4);
				}

				visor.reDraw();
			}

		}

		CListener listener = new CListener();

		tabbedPane.addChangeListener(listener);


		return tabbedPane;

	}

	public JPanel createPerspectivePanel(){
		
		final JLabel sliderLabel1 = new JLabel("D = "+ visor.getD(), JLabel.CENTER);

		JSlider dSlider = new JSlider(JSlider.HORIZONTAL,
				0, 50, 50);

		dSlider.setMajorTickSpacing(10);
		dSlider.setPaintTicks(true);
		dSlider.setPaintLabels(true);


		class SListener implements ChangeListener {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				
					visor.setD(source.getValue());
					if(visor.getD() == 0){
						visor.setD(1);
						source.setValue(1);
					}
					sliderLabel1.setText("D = "+ visor.getD());
					visor.reDraw();
			}
		}

		SListener listener = new SListener();

		dSlider.addChangeListener(listener);
		JPanel controlPanel = new JPanel();
		controlPanel.add(sliderLabel1);
		controlPanel.add(dSlider);

		return controlPanel;
	}

	public JPanel createAxionometricPanel(){
		final JLabel sliderLabel1 = new JLabel("A = "+ visor.getA(), JLabel.CENTER);
		final JLabel sliderLabel2 = new JLabel("B = "+ visor.getB(), JLabel.CENTER);
		JSlider aSlider = new JSlider(JSlider.HORIZONTAL,
				0, 90, 24);

		aSlider.setMajorTickSpacing(45);
		aSlider.setPaintTicks(true);
		aSlider.setPaintLabels(true);
		aSlider.setName("A");



		JSlider bSlider = new JSlider(JSlider.HORIZONTAL,
				0, 90, 17);

		bSlider.setMajorTickSpacing(15);
		bSlider.setPaintTicks(true);
		bSlider.setPaintLabels(true);
		bSlider.setName("B");


		class SListener implements ChangeListener {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				
					int value = source.getValue();

					if(source.getName().equals("A")){
						if(visor.getB()+value <= 90){
							visor.setA(value);
							sliderLabel1.setText("A = "+ visor.getA());
						}
						else source.setValue((int) visor.getA());

					}
					else{
						if(visor.getA()+value <= 90){
							visor.setB(value);
							sliderLabel2.setText("B = "+ visor.getB());
						}
						else { source.setValue((int) visor.getB());
								System.out.println("Slider should be stuck!");
						}
					}

					visor.reDraw();
			}
		}

		SListener listener = new SListener();

		aSlider.addChangeListener(listener);
		bSlider.addChangeListener(listener);
		JPanel controlPanel = new JPanel();
		controlPanel.add(sliderLabel1);
		controlPanel.add(aSlider);
		controlPanel.add(sliderLabel2);
		controlPanel.add(bSlider);
		return controlPanel;
	}

	public JPanel createOrtogonalPanel(){
		ButtonGroup group = new ButtonGroup();

		JRadioButton planta = new JRadioButton();
		planta.setText("Planta");
		planta.setActionCommand("Planta");
		group.add(planta);

		JRadioButton alcadoP = new JRadioButton();
		alcadoP.setText("Al�ado Principal");
		alcadoP.setActionCommand("Al�ado Principal");
		alcadoP.setSelected(true);
		group.add(alcadoP);

		JRadioButton alcadoLE = new JRadioButton();
		alcadoLE.setText("Al�ado Lateral Esquerdo");
		alcadoLE.setActionCommand("Al�ado Lateral Esquerdo");
		group.add(alcadoLE);

		JRadioButton alcadoLD = new JRadioButton();
		alcadoLD.setText("Al�ado Lateral Direito");
		alcadoLD.setActionCommand("Al�ado Lateral Direito");
		group.add(alcadoLD);

		JPanel controlPanel = new JPanel();
		controlPanel.add(planta);
		controlPanel.add(alcadoP);
		controlPanel.add(alcadoLE);
		controlPanel.add(alcadoLD);

		class AListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().equals("Al�ado Principal")){
					visor.setOrtoType(1);
				}
				else if(e.getActionCommand().equals("Planta")){
					visor.setOrtoType(2);
				}
				else if(e.getActionCommand().equals("Al�ado Lateral Esquerdo")){
					visor.setOrtoType(3);
				}
				else if(e.getActionCommand().equals("Al�ado Lateral Direito")){
					visor.setOrtoType(4);
				}
				
				visor.reDraw();
			}

		}

		AListener listener = new AListener();

		planta.addActionListener(listener);
		alcadoP.addActionListener(listener);
		alcadoLE.addActionListener(listener);
		alcadoLD.addActionListener(listener);

		return controlPanel;		

	}

	public JPanel createObliquePanel() {
		
		final JLabel sliderLabel1 = new JLabel("\u2113 = " + visor.getL(), JLabel.CENTER);
		final JLabel sliderLabel2 = new JLabel("\u03B1 = "+ visor.getAlpha(), JLabel.CENTER);
		JSlider lSlider = new JSlider(JSlider.HORIZONTAL,
				MINL, MAXL, DEFL);

		Hashtable<Integer, JLabel> LSliderLabeling = new Hashtable<Integer, JLabel>();
		
		LSliderLabeling.put(0, new JLabel("0"));
		LSliderLabeling.put(25, new JLabel("0.25"));
		LSliderLabeling.put(50, new JLabel("0.50"));
		LSliderLabeling.put(75, new JLabel("0.75"));
		LSliderLabeling.put(100, new JLabel("1"));
		
		lSlider.setLabelTable(LSliderLabeling);
		
		lSlider.setMajorTickSpacing(10);
		lSlider.setMinorTickSpacing(5);
		lSlider.setPaintTicks(true);
		lSlider.setPaintLabels(true);

		JSlider aSlider = new JSlider(JSlider.HORIZONTAL,
				0, 360, 63);

		aSlider.setMajorTickSpacing(90);
		aSlider.setPaintTicks(true);
		aSlider.setPaintLabels(true);


		class SListener implements ChangeListener {

			@Override
			public void stateChanged(ChangeEvent e) {

				JSlider source = (JSlider) e.getSource();

				int value = source.getValue();

				if(source.getMaximum()==100){
					double d = ((float)value)/100;
					visor.setL((float)(d));
					sliderLabel1.setText("\u2113 = " + visor.getL());
				}
				else{
					visor.setAlpha(value);
					sliderLabel2.setText("\u03B1 = "+ visor.getAlpha());
				}

				visor.reDraw();
			}
		}

		SListener listener = new SListener();

		lSlider.addChangeListener(listener);
		aSlider.addChangeListener(listener);
		JPanel controlPanel = new JPanel();
		controlPanel.add(sliderLabel1);
		controlPanel.add(lSlider);
		controlPanel.add(sliderLabel2);
		controlPanel.add(aSlider);
		return controlPanel;
	}

	public JMenu createFileMenu(){
		
		JMenu menu;
		JMenuItem reset, load;
		
		menu = new JMenu("File");
		reset = new JMenuItem("Reset");
		load = new JMenuItem("Import...");
		
		class ListenerMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent event)
			{
				if(event.getActionCommand().equals("Import...")){
					JFileChooser chooser = new JFileChooser();
					
					chooser.setDialogTitle("Choose a file");
					chooser.setDialogType(JFileChooser.OPEN_DIALOG);
					chooser.showOpenDialog(V3DFrame.this);
					try{
						visor.load(chooser.getSelectedFile());
					}catch (Exception e) {
						JOptionPane.showMessageDialog(visor, "Nothing loaded");
						e.printStackTrace();
					}
				}
				
				if(event.getActionCommand().equals("Reset")){
					visor.clear();
				}
			}
		}

		load.addActionListener(new ListenerMenu());
		reset.addActionListener(new ListenerMenu());
		menu.add(reset);
		menu.add(load);
		return menu;
	}

}
